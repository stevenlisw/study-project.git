package com.lucas.shopping.seckill.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("lucas_product")
public class Product {


    @TableId
    private Long id;

    private String name;

    private Integer stock;
}
