package com.lucas.common.result;

import lombok.Data;

/**
 * @auther Lucas & lisw
 * @date 2020/12/12 22:52
 */
public enum  ResultCode {

    //成功
    SUCCESS(0,"返回成功"),

    //失败
    ERROR(1,"请求异常"),

    //404
    NOT_FOUND(404,"未找到接口地址");


    ResultCode(int code,String msg){
        this.code = code;
        this.msg = msg;
    }
    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
