package com.lucas.blogwebgicserver.csdn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lucas.blogwebgicserver.csdn.model.Articles;

/**
 * @author lisw
 * @program study-project
 * @description
 * @createDate 2021-01-18 15:07:09
 * @slogan 长风破浪会有时，直挂云帆济沧海。
 **/
public interface CsdnArticlesService extends IService<Articles> {
}
