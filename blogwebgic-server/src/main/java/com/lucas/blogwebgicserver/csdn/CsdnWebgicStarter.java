package com.lucas.blogwebgicserver.csdn;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lucas.blogwebgicserver.utils.HttpUtils;
import com.lucas.common.utils.Unicode2utf8Utils;

/**
 * @author lisw
 * @program study-project
 * @description
 * @createDate 2021-01-18 09:55:46
 * @slogan 长风破浪会有时，直挂云帆济沧海。
 **/
public class CsdnWebgicStarter {


    public static void main(String[] args) {
        for(int i =0;i<10;i++){
            String result =   HttpUtils.sendGet("https://www.csdn.net/api/articles?type=more&category=java","");
            JSONObject rtnJson = JSON.parseObject(result);
            JSONArray articlesArray = rtnJson.getJSONArray("articles");

            System.out.println(Unicode2utf8Utils.convert(result));
        }
    }
}
