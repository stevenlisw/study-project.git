package com.lucas.blogwebgicserver.csdn.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lucas.blogwebgicserver.csdn.mapper.CsdnArticlesMapper;
import com.lucas.blogwebgicserver.csdn.model.Articles;
import com.lucas.blogwebgicserver.csdn.service.CsdnArticlesService;
import org.springframework.stereotype.Service;

/**
 * @author lisw
 * @program study-project
 * @description
 * @createDate 2021-01-18 15:48:23
 * @slogan 长风破浪会有时，直挂云帆济沧海。
 **/
@Service
public class CsdnArticlesServiceImpl extends ServiceImpl<CsdnArticlesMapper, Articles> implements CsdnArticlesService {
}
