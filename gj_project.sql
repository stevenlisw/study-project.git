/*
 Navicat Premium Data Transfer

 Source Server         : xiaowu
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-uf6f7xh7p91q122hipo.mysql.rds.aliyuncs.com:3306
 Source Schema         : gj_project

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 17/01/2021 13:32:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for lucas_product
-- ----------------------------
DROP TABLE IF EXISTS `lucas_product`;
CREATE TABLE `lucas_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1337042177853804546 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of lucas_product
-- ----------------------------
BEGIN;
INSERT INTO `lucas_product` VALUES (1337041728065032193, 'iphone12', 0);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
