package com.lucas.blogwebgicserver.csdn.web;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lucas.blogwebgicserver.csdn.model.Articles;
import com.lucas.blogwebgicserver.csdn.service.CsdnArticlesService;
import com.lucas.blogwebgicserver.utils.HttpUtils;
import com.lucas.common.result.ResultRtn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lisw
 * @program study-project
 * @description
 * @createDate 2021-01-18 15:53:31
 * @slogan 长风破浪会有时，直挂云帆济沧海。
 **/
@RestController
public class ArticlesController {

    @Autowired
    private CsdnArticlesService articlesService;



    @GetMapping("/syn/csdn/{id}")
    public ResultRtn secKill(@PathVariable("id") Integer count) {
        List<Articles> articlesList = new ArrayList<>();
        for(int i =0;i<count;i++){
            String result =   HttpUtils.sendGet("https://www.csdn.net/api/articles?type=more&category=java","");
            JSONObject rtnJson = JSON.parseObject(result);
            JSONArray articlesArray = rtnJson.getJSONArray("articles");
            for(int k= 0;k<articlesArray.size();k++){
                JSONObject obj = articlesArray.getJSONObject(k);
                Articles articles =new Articles();
                articles.setComments(obj.getInteger("comments"));
                articles.setViews(obj.getInteger("views"));
                articles.setCsdnId(obj.getString("id"));
                articles.setTitle(obj.getString("title"));
                articles.setUrl(obj.getString("url"));
                articlesList.add(articles);
            }
        }
        if(articlesList!=null && articlesList.size()>0){
            articlesService.saveBatch(articlesList);
        }
        return ResultRtn.ok("");
    }
}
