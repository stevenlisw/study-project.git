package com.lucas.blogwebgicserver.csdn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lucas.blogwebgicserver.csdn.model.Articles;

/**
 * @author lisw
 * @program study-project
 * @description
 * @createDate 2021-01-18 15:49:23
 * @slogan 长风破浪会有时，直挂云帆济沧海。
 **/
public interface CsdnArticlesMapper extends BaseMapper<Articles> {
}
