package com.lucas.shopping.config;

/**
 * @auther Lucas & lisw
 * @date 2021/1/14 22:51
 */
public class Constants {

    //zookeeper 商品Key的前缀
    public final static String zoo_product_key_prefix = "/zoo_product_stock";

    //Redis 商品Key的前缀
    public final static String redis_product_key_prefix = "product:stock:";

    //本地缓存商品Key的前缀
    public final static String local_product_key_prefix = "product_";
}
