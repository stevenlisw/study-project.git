   local isExist = redis.call('exists', KEYS[1])  -- 判断商品是否存在
local isUserId = redis.call('exists', KEYS[2])  -- 判断此用户是否已经抢过
    if isExist == 1 then
        local goodsNumber = redis.call('get', KEYS[1])        -- 获取商品的数量
        if goodsNumber > "0" then
            if isUserId == 0 then --没有抢过（防止一个人多抢）
                 redis.call('decr', KEYS[1])                       -- 如果商品数量大于0，则库存减1
                 redis.call('set', KEYS[2],"1")     --此人抢成功，记录，再次抢购，将不会在成功
                 return 1
            else
                 return 2
            end
        else
            --redis.call("del", KEYS[1])                         商品数量为0则从秒杀活动删除该商品
            return 0
        end
    else return -1
end