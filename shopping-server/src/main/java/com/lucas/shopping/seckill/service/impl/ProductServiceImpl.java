package com.lucas.shopping.seckill.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lucas.shopping.seckill.entity.Product;
import com.lucas.shopping.seckill.mapper.ProductMapper;
import com.lucas.shopping.seckill.service.ProductService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProductServiceImpl extends ServiceImpl<ProductMapper,Product> implements ProductService {

    @Override
    public void updateStock(Long id) {
        int rows = baseMapper.updateStock(id);
        if(rows == 0 ){
            throw  new RuntimeException("此商品已经售完");
        }
    }
}
