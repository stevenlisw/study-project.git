package com.lucas.common.utils;

/**
 * @author lisw
 * @program study-project
 * @description
 * @createDate 2021-01-11 11:15:45
 * @slogan 别人笑我太疯癫，我笑他人看不穿； 长风破浪会有时，直挂云帆济沧海。
 **/
public class Unicode2utf8Utils {

    public static String convert(String unicodeString) {
        StringBuilder stringBuilder = new StringBuilder();
        int i = -1;
        int pos = 0;
        while ((i = unicodeString.indexOf("\\u", pos)) != -1) {
            stringBuilder.append(unicodeString.substring(pos, i));
            if (i + 5 < unicodeString.length()) {
                pos = i + 6;
                stringBuilder.append((char) Integer.parseInt(unicodeString.substring(i + 2, i + 6), 16));
            }
        }
        return stringBuilder.toString();
    }
}
