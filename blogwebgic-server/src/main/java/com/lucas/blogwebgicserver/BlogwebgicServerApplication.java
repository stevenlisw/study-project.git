package com.lucas.blogwebgicserver;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(scanBasePackages = {"com.lucas"})
@MapperScan("com.lucas.**.mapper")
public class BlogwebgicServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlogwebgicServerApplication.class, args);
    }

}
