package com.lucas.shopping.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lucas.shopping.seckill.entity.Product;
import org.apache.ibatis.annotations.Param;

public interface ProductMapper extends BaseMapper<Product> {

    int updateStock(@Param("id") Long id);
}
