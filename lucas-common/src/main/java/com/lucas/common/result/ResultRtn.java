package com.lucas.common.result;

import lombok.Data;

@Data
public class ResultRtn<T> {

    private int code;

    private String msg;

    private T data;



    public static ResultRtn ok(String data){
       ResultRtn resultRtn = new ResultRtn();
       resultRtn.setData(data);
       resultRtn.setCode(ResultCode.SUCCESS.getCode());
       resultRtn.setMsg(data);
       return resultRtn;
    }

    public static ResultRtn error(String data){
        ResultRtn resultRtn = new ResultRtn();
        resultRtn.setCode(ResultCode.ERROR.getCode());
        resultRtn.setMsg(data);
        return resultRtn;
    }

}
