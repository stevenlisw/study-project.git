package com.lucas.shopping.seckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lucas.shopping.seckill.entity.Product;
import org.apache.ibatis.annotations.Param;

public interface ProductService extends IService<Product> {
    void updateStock(@Param("id") Long id);
}
