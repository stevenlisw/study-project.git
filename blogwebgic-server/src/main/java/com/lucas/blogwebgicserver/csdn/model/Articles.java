package com.lucas.blogwebgicserver.csdn.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * @author lisw
 * @program study-project
 * @description
 * @createDate 2021-01-18 14:55:07
 * @slogan 长风破浪会有时，直挂云帆济沧海。
 **/
@Data
@TableName("csdn_blog")
public class Articles {

    @TableId(type = IdType.AUTO)
    private Integer id;

    @TableField(value = "title")
    private String title;
    @TableField(value = "url")
    private String url;
    @TableField(value = "csdn_id")
    private String csdnId;
    @TableField(value = "comments")
    private Integer comments;
    @TableField(value = "views")
    private Integer views;
}
