# 基于Redis+Zookeeper+MySQL分布式秒杀系统

> **作者Lucas**   
> 使用到的技术栈：  
> SpringBoot、Mybatis、Redis、MySQL、SpringCloud AliBaBa
> Zookeeper、Nacos。

**使用须知**



1. 将项目中gj_config.sql及gi_project.sql导入数据库。

2. 中间件准备及启动  
自行下载 Nacos及Zookeeper。
Nacos启动前，需配置Nacos的数据源。修改Nacos下的config目录下的application.properties文件

```
nacos.istio.mcp.server.enabled=false
spring.datasource.platform=mysql
 
db.num=1
db.url.0=jdbc:mysql://127.0.0.1:3306/gj_config?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true
db.user.0=root
db.password.0=root
```

### 感谢支持与关注
### 欢迎一起参与讨论与交流
### 加我微信，进交流群
![mmqrcode1610862757073.png](https://i.loli.net/2021/01/17/gonPYuDRzUxlAJT.jpg)
### 关注微信公众号"程序员无名",一起交流学习，了解更多技术知识
![430430.jpg](https://cdnjson.com/images/2023/04/03/430430.jpg)
